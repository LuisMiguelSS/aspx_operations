﻿<%@ Page Title="" Language="C#" MasterPageFile="~/layout.Master" AutoEventWireup="true" CodeBehind="numeros.aspx.cs" Inherits="Operaciones.WebForm1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">

        <!-- First input -->
        <div class="input-field col s3">
            <asp:TextBox ID="textNumber1" runat="server" ToolTip="Inserta el primer número"></asp:TextBox>
            <label>Número 1</label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="textNumber2" ErrorMessage="¡Debes rellenar el primer campo!" Display="None"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="textNumber2" Display="None" ErrorMessage="¡Sólo se permiten números y decimales en el primer campo!" ValidationExpression="^(\+|\-?\d+)(\.|\,\d+)?$"></asp:RegularExpressionValidator>
        </div>

        <!-- Second input -->
        <div class="input-field col s3">
            <asp:TextBox ID="textNumber2" runat="server" ToolTip="Inserta el segundo número"></asp:TextBox>
            <label>Número 2</label>
            <asp:RequiredFieldValidator ID="secondNumberRequiredValidator" runat="server" ControlToValidate="textNumber2" ErrorMessage="¡Debes rellenar el segundo campo!" Display="None"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexNumber2" runat="server" ControlToValidate="textNumber2" Display="None" ErrorMessage="¡Sólo se permiten números y decimales en el primer campo!" ValidationExpression="^(\+|\-?\d+)(\.|\,\d+)?$"></asp:RegularExpressionValidator>
        </div>

        <!-- Calculation buttons -->
        <div class="input-field col s3">
            <asp:Button ID="btnMultiply" runat="server" Text="x" class="btn white black-text" OnClick="btn_CalculateResult_Click" />
            <asp:Button ID="btnDivide" runat="server" Text="/" class="btn white black-text" OnClick="btn_CalculateResult_Click" />
        </div>
        <div class="input-field col s3">
            <asp:Button ID="btnSum" runat="server" Text="+" class="btn white black-text" OnClick="btn_CalculateResult_Click" />
            <asp:Button ID="btnSubtract" runat="server" Text="-" class="btn white black-text" OnClick="btn_CalculateResult_Click" />
        </div>
        
    </div>

    <!-- Result Textbox -->
    <div class="row">
        <div class="input-field col s6">
            <asp:TextBox ID="textResult" runat="server"></asp:TextBox>
            <label>Resultado</label>
        </div>
    </div>

    <!-- Error Validation Container -->
    <asp:ValidationSummary ID="errorSummary" runat="server" class="materialert" DisplayMode="BulletList" HeaderText="<b>Errores</b><button type='button' class='close-alert' onclick='closeErrors()'>×</button>" />

</asp:Content>
