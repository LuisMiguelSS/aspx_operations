﻿<%@ Page Title="" Language="C#" MasterPageFile="~/layout.Master" AutoEventWireup="true" CodeBehind="letras.aspx.cs" Inherits="Operaciones.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">

        <!-- First input -->
        <div class="input-field col s3">
            <asp:TextBox ID="textBoxTextInput" runat="server" ToolTip="Inserta texto aquí"></asp:TextBox>
            <label>Texto Original</label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="textBoxTextInput" ErrorMessage="¡Debes rellenar el primer campo!" Display="None"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="validatorText1" runat="server" ControlToValidate="textBoxTextInput" Display="None" ErrorMessage="¡Sólo se permiten palabras y espacios en el primer campo!" ValidationExpression="^([A-zÀ-ú]+ *)+$"></asp:RegularExpressionValidator>
        </div>

        <!-- Second input -->
        <div class="input-field col s1">
            <asp:TextBox ID="textBoxLetter" runat="server" ToolTip="Inserta la letra"></asp:TextBox>
            <label>Letra</label>
            <asp:RequiredFieldValidator ID="secondNumberRequiredValidator" runat="server" ControlToValidate="textBoxLetter" ErrorMessage="¡Debes rellenar el segundo campo!" Display="None"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="validatorText2" runat="server" ControlToValidate="textBoxLetter" Display="None" ErrorMessage="¡Sólo se permiten palabras y espacios en el primer campo!" ValidationExpression="^([A-zÀ-ú]+ *)+$"></asp:RegularExpressionValidator>
        </div>

        <!-- Calculation buttons -->
        <div class="input-field col s5">
            <asp:Button ID="btnCount" runat="server" Text="Repeticiones" class="btn white black-text" OnClick="btnCount_Click" />
            <asp:Button ID="btnRemove" runat="server" Text="Eliminar" class="btn white black-text" OnClick="btnRemove_Click" />
        </div>
        
    </div>

    <!-- Result Textbox -->
    <div class="row">
        <div class="input-field col s6">
            <asp:TextBox ID="textResult" runat="server"></asp:TextBox>
            <label>Resultado</label>
        </div>
    </div>

    <!-- Error Validation Container -->
    <asp:ValidationSummary ID="errorSummary" runat="server" class="materialert" DisplayMode="BulletList" HeaderText="<b>Errores</b><button type='button' class='close-alert' onclick='closeErrors()'>×</button>" />
</asp:Content>
