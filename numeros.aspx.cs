﻿using System;

namespace Operaciones
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        public double PrepareForCalc(string text)
        {
            return Convert.ToDouble(text.Trim().Replace(".", ","));
        }

        //
        // Listener
        //
        protected void btn_CalculateResult_Click(object sender, EventArgs e)
        {
            string result = "";

            double number1 = PrepareForCalc(textNumber1.Text);
            double number2 = PrepareForCalc(textNumber2.Text);

            switch ((sender as System.Web.UI.WebControls.Button).Text)
            {
                case "+":
                    result = number1 + number2 + "";
                    break;
                case "-":
                    result = number1 - number2 + "";
                    break;
                case "x":
                    result = number1 * number2 + "";
                    break;
                case "/":
                    result = (number2 == 0) ? "0" : number1 / number2 + "";
                    break;
            }

            textResult.Text = result;
        }

    }
}