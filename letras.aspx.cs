﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Operaciones
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        //
        // Listeners
        //
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnCount_Click(object sender, EventArgs e)
        {
            textResult.Text = Regex.Matches(textBoxTextInput.Text, textBoxLetter.Text).Count + "";
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            textResult.Text = textBoxTextInput.Text.Replace(textBoxLetter.Text, "");
        }
    }
}