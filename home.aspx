﻿<%@ Page Title="" Language="C#" MasterPageFile="~/layout.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="Operaciones.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="slider">
            <ul class="slides">
                <li>
                    <img src="https://i.pinimg.com/originals/07/cb/f3/07cbf35c8d1d78109e08df4aee041d0c.jpg">
                    <div class="caption left-align">
                        <h3>¡Bienvenido/a!</h3>
                        <h5 class="light grey-text text-lighten-3">¡Usa el menú superior para continuar!</h5>
                    </div>
                </li>
                <li>
                    <img src="https://wallup.net/wp-content/uploads/2017/03/29/486747-monochrome-computer-programming-HTML-depth_of_field-monitor-web_development-code-photography-blurred-748x421.jpg">
                    <div class="caption left-align">
                        <h3>Aspx es una maravilla</h3>
                        <h5 class="light grey-text text-lighten-3">Aunque por supuesto no tanto como Python.</h5>
                    </div>
                </li>
            </ul>
        </div>
    <div class="container white black-text center-align" id="about">
        <h3>Sobre este proyecto</h3>
        <hr />
        <p>Principalmente, las validaciones están realizadas con controles del tipo:</p>
        <ul>
            <li>RequiredFieldValidator</li>
            <li>RegularExpressionValidator</li>
        </ul>
        <p>Usando expresiones del tipo <code class="grey lighten-3">^(\+|\-?\d+)(\.|\,\d+)?$</code> para admitir números positivos, negativos y con o sin decimales, así como <code class="grey lighten-3">^([A-zÀ-ú]+ *)+$</code> para el texto con espacios y acentos.</p>
        <p>Asimismo ha sido estilizado usando el framework de <a class="materialize" href="https://materializecss.com/" target="_blank">Materialize CSS</a>.</p>
    </div>
</asp:Content>
